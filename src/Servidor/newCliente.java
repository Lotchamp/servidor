package Servidor;


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Davis
 */
public class newCliente extends Thread {
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private BufferedReader msg;
    InputStreamReader inSocket;
    private String name;
    
    public newCliente(Socket Socket, DataOutputStream Out){
        this.socket = Socket;
        this.out = Out;
    }
    
    public void desconectar(){
        try{
            this.socket.close();
        }catch (IOException ex){    }
    }
    
    @Override
    public void run(){
        String text="";
        try{
            in = new DataInputStream(socket.getInputStream());
            inSocket = new InputStreamReader(socket.getInputStream());
            msg = new BufferedReader(inSocket);
            out = new DataOutputStream(socket.getOutputStream());
            
            while((text = msg.readLine()) != null ){
                if(text.equals("salir")){
                    break;
                }else{
                    System.out.println(""+text);
                    VentanaServidor.jTextArea1.setText(VentanaServidor.jTextArea1.getText()+"\n"+text);
                    Main.servidor.enviarMensaje(""+text);
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(newCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    

}
